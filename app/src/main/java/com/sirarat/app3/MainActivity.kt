package com.sirarat.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main);
        val arrayAdapter: ArrayAdapter<*>
        val oilName = arrayOf(
            "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95",
            "เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", "เชลล์ ดีเซล B20", "เชลล์ ฟิวเซฟ ดีเซล",
            "เชลล์ ฟิวเซฟ ดีเซล B7", "เชลล์ วี-เพาเวอร์ ดีเซล", "เชลล์ วี-เพาเวอร์ ดีเซล B7"
        )
        // access the listView from xml file
        var mListView = findViewById<ListView>(R.id.listView)
        arrayAdapter = ArrayAdapter(this,
            android.R.layout.simple_list_item_1, oilName)
        mListView.adapter = arrayAdapter






    }
}